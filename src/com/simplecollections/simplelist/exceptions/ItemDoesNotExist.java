package com.simplecollections.simplelist.exceptions;

public class ItemDoesNotExist extends Exception {
    public ItemDoesNotExist() {
        super("Такого Item не существует");
    }
}
