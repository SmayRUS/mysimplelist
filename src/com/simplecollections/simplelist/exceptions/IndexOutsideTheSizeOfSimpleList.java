package com.simplecollections.simplelist.exceptions;

public class IndexOutsideTheSizeOfSimpleList extends Exception {
    public IndexOutsideTheSizeOfSimpleList(int index, int size) {
        super("Индекс " + index + " выходит за границы com.simplecollections.simplelist.SimpleList: размер " + size);
    }
}
