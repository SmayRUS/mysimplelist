package com.simplecollections.simplelist;

import java.util.Comparator;
import java.util.Random;

public class Main {
    public static void main(String args[]) {
        int n = 10000;
        SimpleList<Integer> list = new SimpleArrayList<>();

        Random randomizer = new Random();

        for ( int i = 0; i < n; i++) {
            list.add(randomizer.nextInt( 1000000 ));
        }

        list = list.shuffle();

        Comparator<Integer> comparator = getIntegerComparatorForSimpleList();

        SimpleList<Integer> quickSortMultiThreadingList = list.quickSortMultiThreading(comparator);
        SimpleList<Integer> arraySortList = list.arraySort();
        SimpleList<Integer> bubbleSortList = list.sortBubble(comparator);

//        System.out.println(list);
//        System.out.println("");
//        System.out.println(quickSortMultiThreadingList);
    }

    public static Comparator<Integer> getIntegerComparatorForSimpleList() {
        return new Comparator<Integer>() {
            @Override
            public int compare(Integer t, Integer t1) {
                return t.compareTo(t1);
            }
        };
    }

}
