package com.simplecollections.simplelist;

import com.simplecollections.simplelist.exceptions.IndexOutsideTheSizeOfSimpleList;

import java.util.Comparator;
import java.util.Optional;

public interface SimpleList<T> extends Iterable<T> {
    void add(T item);
    void insert(int index, T item) throws IndexOutsideTheSizeOfSimpleList;
    void remove(int index) throws IndexOutsideTheSizeOfSimpleList;
    Optional<T> get(int index);
    int size();
    void addAll(SimpleList<T> list);
    int first(T item);
    int last(T item);
    boolean contains(T item);
    boolean isEmpty();
    SimpleList<T> shuffle();
    SimpleList<T> sortBubble(Comparator<T> comparator);
    SimpleList<T> quickSortMultiThreading(Comparator<T> comparator);
    SimpleList<T> arraySort();
}
