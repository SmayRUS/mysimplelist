package com.simplecollections.simplelist;

import com.simplecollections.simplelist.exceptions.IndexOutsideTheSizeOfSimpleList;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ThreadLocalRandom;

public class SimpleArrayList<T> implements SimpleList<T> {
    private final int INIT_SIZE = 10;

    private Object[] array;

    private int size = 0;

    public SimpleArrayList() {
        array = new Object[INIT_SIZE];
    }

    @Override
    public void add(T item) {
        sizeCheck(size);
        array[size++] = item;
    }

    @Override
    public void insert(int index, T item) throws IndexOutsideTheSizeOfSimpleList{
        if (index > size) throw new IndexOutsideTheSizeOfSimpleList(index, size);

        sizeCheck(size);
        size++;
        for (int i = size; i > index; i--)
            array[i] = array[i - 1];
        array[index] = item;
    }

    @Override
    public void remove(int index) throws IndexOutsideTheSizeOfSimpleList {
        if (index > size) throw new IndexOutsideTheSizeOfSimpleList(index, size);

        for (int i = index; i < size; i++)
            array[i] = array[i + 1];
        array[size] = null;
        size--;
    }

    @Override
    public Optional<T> get(int index) {
        return (Optional<T>) Optional.ofNullable(array[index]);
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void addAll(SimpleList<T> list) {
        for (T t : list) {
            sizeCheck(size);
            array[size] = t;
            size++;
        }
    }

    @Override
    public int first(T item) {
        for (int index = 0; index < size; index++) {
            if (array[index].equals(item))
                return index;
        }

        return -1;
    }

    @Override
    public int last(T item) {
        for (int index = size; index > 0; index--) {
            if (array[index].equals(item))
                return index;
        }

        return -1;
    }

    @Override
    public boolean contains(T item) {
        for (int index = 0; index < size; index++) {
            if (array[index].equals(item))
                return true;
        }

        return false;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public SimpleList<T> shuffle() {
        Object[] shuffleObject = createInputObject();

        Random rnd = ThreadLocalRandom.current();
        for (int index2 = size - 1; index2 > 0; index2--) {
            int index1 = rnd.nextInt(index2 + 1);
            swap(shuffleObject, index1, index2);
        }

        return convertObjToSimpleList(shuffleObject);
    }

    @Override
    public SimpleList<T> sortBubble(Comparator<T> comparator) {
        Object[] sortObject = createInputObject();

        boolean needIteration = true;

        long start = System.currentTimeMillis();
        while (needIteration) {
            needIteration = false;

            for (int index = 1; index < size; index++) {
                if (comparator.compare((T) sortObject[index], (T) sortObject[index - 1]) == -1) {
                    swap(sortObject, index, index - 1);

                    needIteration = true;
                }
            }
        }
        long finish = System.currentTimeMillis();
        long elapsed = finish - start;

        System.out.println("Time BubbleSort: " + elapsed + " milliseconds");

        return convertObjToSimpleList(sortObject);
    }

    @Override
    public SimpleList<T> quickSortMultiThreading(Comparator<T> comparator) {
        Object[] sortObject = createInputObject();

        ForkJoinPool pool = ForkJoinPool.commonPool();

        long start = System.currentTimeMillis();
        pool.invoke(new QuickSortMultiThreading(0, size - 1, sortObject, comparator));
        long finish = System.currentTimeMillis();
        long elapsed = finish - start;

        System.out.println("Time QuickSortMultiThreading: " + elapsed + " milliseconds");

        return convertObjToSimpleList(sortObject);
    }

    @Override
    public SimpleList<T> arraySort() {
        Object[] sortObject = createInputObject();

        long start = System.currentTimeMillis();
        Arrays.sort(sortObject);
        long finish = System.currentTimeMillis();
        long elapsed = finish - start;

        System.out.println("Time ArraySort: " + elapsed + " milliseconds");

        return convertObjToSimpleList(sortObject);
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayIterator<>((T[]) array);
    }

    private void resize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array, 0, newArray, 0, size);
        array = newArray;
    }

    private void sizeCheck (int size) {
        if (size == array.length-1)
            resize((int)Math.ceil(array.length * 2 + 1));
    }

    private void swap (Object[] obj, int index1, int index2) {
        T a = (T) obj[index1];
        obj[index1] = obj[index2];
        obj[index2] = a;
    }

    private Object[] createInputObject () {
        int sizeResult = Math.max(INIT_SIZE, size);

        Object[] inputObject = new Object[sizeResult];

        for (int i = 0; i < size; i++)
            inputObject[i] = array[i];

        return inputObject;
    }

    private SimpleList<T> convertObjToSimpleList (Object[] obj) {
        SimpleList<T> result = new SimpleArrayList<>();

        for (int i = 0; i < size; i++)
            result.add((T) obj[i]);

        return result;
    }

    @Override
    public String toString() {
        return Arrays.toString(array) +
                ", size=" + size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleArrayList)) return false;
        SimpleArrayList<?> that = (SimpleArrayList<?>) o;
        return INIT_SIZE == that.INIT_SIZE && size == that.size && Arrays.equals(array, that.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(INIT_SIZE, size);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }
}
