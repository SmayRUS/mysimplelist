package com.simplecollections.simplelist;

import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.RecursiveTask;

class QuickSortMultiThreading<T> extends RecursiveTask<Integer> {

    int start;
    int end;
    Object[] arr;
    Comparator<T> comparator;

    private int partition(int start, int end, Object[] arr, Comparator<T> comparator) {
        int i = start, j = end;

        int pivote = new Random().nextInt(j - i) + i;

        T t = (T) arr[j];
        arr[j] = arr[pivote];
        arr[pivote] = t;
        j--;

        while (i <= j) {

//            if (arr[i] <= arr[end]) {
            if (comparator.compare((T) arr[i], (T) arr[end]) == -1) {
                i++;
                continue;
            }

//            if (arr[j] >= arr[end]) {
            if (comparator.compare((T) arr[j], (T) arr[end]) == 1) {
                j--;
                continue;
            }

            t = (T) arr[j];
            arr[j] = arr[i];
            arr[i] = t;
            j--;
            i++;
        }

        t = (T) arr[j + 1];
        arr[j + 1] = arr[end];
        arr[end] = t;
        return j + 1;
    }

    public QuickSortMultiThreading(int start, int end, Object[] arr, Comparator<T> comparator) {
        this.arr = arr;
        this.start = start;
        this.end = end;
        this.comparator = comparator;
    }

    @Override
    protected Integer compute() {
        if (start >= end)
            return null;

        int p = partition(start, end, arr, comparator);

        QuickSortMultiThreading left = new QuickSortMultiThreading(start,p - 1, arr, comparator);

        QuickSortMultiThreading right = new QuickSortMultiThreading(p + 1, end, arr, comparator);

        left.fork();
        right.compute();
        left.join();

        return null;
    }
}

